const express = require('express');
const app = express();
const port = 3000;
const mylib = require('./mylib.js');
app.get('/', (req, res) => {
    res.send('Hello world');
});
app.get('/add', (req, res) => {
    const a = parseInt(req.query.a);
    const b = parseInt(req.query.b);
    res.send(mylib.summultby3(a, b));
});
app.listen(port, () => {
    console.log(`Server: http://localhost:${port}`);
});
