const expect = require('chai').expect;
const mylib = require('../src/mylib');
let myvar = undefined;
const assert = require('assert');
var should = require('chai').should();


describe('Unit testing mylib.js', () => {
    after(() => console.log("After tests"));

    it('Should return 6 when using sum function with a=1, b=1', () => {
        const result = mylib.summultby3(1, 1); // 1 + 1
        expect(result).to.equal(6); // result expected to equal 2
    });

    before(() => {
        myvar = 1; // setup before testing
        console.log('Before testing');
    });
    it('Assert foo is not bar', () => {
        assert('foo' !== 'bar');
    })
    it('Myvar should exist', () => {
        should.exist(myvar)
    })
    it('Arraygen creates a propper array', () => {
        assert(JSON.stringify(mylib.arrayGen())==JSON.stringify([1,2,3]));
    })
});